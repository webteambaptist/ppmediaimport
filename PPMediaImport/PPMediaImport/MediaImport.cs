﻿using Newtonsoft.Json;
using NLog;
using PPMediaImport.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace PPMediaImport
{
    public static class MediaImport
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        //Email Settings
        private static readonly string To = ConfigurationManager.AppSettings["recipient"];
        private static readonly string From = ConfigurationManager.AppSettings["sender"];
        private static readonly string Relay = ConfigurationManager.AppSettings["RELAY"];
        private static readonly string Port = ConfigurationManager.AppSettings["PORT"];
        private static readonly string StrProviderParentId = ConfigurationManager.AppSettings["providerParentID"];
        private static readonly string Dir = ConfigurationManager.AppSettings["InformaticsIn"];
        private static readonly string SitecoreDir = ConfigurationManager.AppSettings["InfomaticsFolderPath"];
        private static readonly string InformaticsOut = ConfigurationManager.AppSettings["InformaticsOut"];
        private static readonly string InfomaticsMicroservice = ConfigurationManager.AppSettings["InfomaticsMicroservice"];
        private const string NewLine = "<br />";
        public static void RunInformaticProcesses()
        {
            try
            {
                Logger.Info("Informatics Timer Elapsed, processing....");
                Logger.Info("Starting Informatics Copy from Sharepoint... ");

                //Get Informatic Files
                GetInformaticsFilesFromMicroservice();

                Logger.Info("Informatic files copied!....");
                Logger.Info("Starting Informatics import to Sitecore...");

                //Copy Informatic Files to Sitecore
                CopyInformaticsToSitecore();

                Logger.Info("Informatics documents imported to Sitecore Finished...");
                Logger.Info("Starting Delete Temp Files....");

                //Delete Temp Files
                DeleteTempFiles();
                Logger.Info("Temp Files Deleted....");
                GC.Collect();
            }
            catch (Exception ex)
            {
                Logger.Error("Exception in RunInformaticProcesses() " + ex.Message);

                var m = new Mail()
                {
                    From = From,
                    To = To,
                    Body = "Exception occurred while running MediaImport.RunInformaticProcesses() " + NewLine + " ex.Message: " +
                               ex.Message + NewLine + " ex.InnerException: " + ex.InnerException + NewLine +
                               "Full Exception: " + ex,
                    Subject = "PPMediaImport Process Failed"
                };

                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                Logger.Info(ex.Message);

            }
        }
        private static void GetInformaticsFilesFromMicroservice()
        {
            var client = new RestClient(InfomaticsMicroservice);
            var request = new RestRequest(Method.GET);
            request.AddHeader("content-type", "application/json");            
            var response = client.Execute(request);
            if (!response.IsSuccessful)
            {
                Console.WriteLine("Error getting Infomatics from sharepoint " + response.ErrorMessage);
                Logger.Error("Error getting Infomatics from sharepoint " + response.ErrorMessage);
                return;
            }
        }

        private static void CopyInformaticsToSitecore()
        {
            try
            {
                var directories = Directory.GetDirectories(Dir);
                foreach (var directory in directories)
                {
                    var fullPath = Path.GetFullPath(directory).TrimEnd(Path.DirectorySeparatorChar);
                    var doc = Path.GetFileName(fullPath);
                    var files = Directory.GetFiles(directory);
                    foreach (var file in files)
                    {
                        try
                        {
                            var name = Path.GetFileName(file);// Path.GetFileNameWithoutExtension(file);
                            Logger.Info("Adding file " + file);
                            AddFile(file, doc, name);
                        }
                        catch (Exception e)
                        {
                            Logger.Error(
                                "Exception occurred for while copying" + doc + " Infomatics copyInformaticsToSitecore() " + e.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                var m = new Mail()
                {
                    From = From,
                    To = To,
                    Body = "Exception occurred while running MediaImport.CopyInformaticsToSitecore() " + NewLine +
                           " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " + ex.InnerException,
                    Subject = "PPMediaImport Process Failed"
                };

                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
              
                Logger.Error(
                    "Exception occurred in CopyInformaticsToSitecore() " + ex.Message);
            }

        }

        private static void DeleteTempFiles()
        {
            foreach (var folder in System.IO.Directory.GetDirectories(InformaticsOut))
            {
                try
                {
                    Logger.Info("Deleting Folder " + folder);
                    Directory.Delete(folder, true);
                }
                catch (Exception e)
                {
                    Logger.Error("Exception deleting folder " + folder + " " + e.Message);
                }
            }
        }

        private static void AddFile(string fileName, string doc, string mediaItemName)
        {
            try
            {
                 fileName = Path.GetFullPath(fileName);
                var docDir = SitecoreDir + @"\" + doc;
                var exists = Directory.Exists(docDir);
                if (!exists)
                {
                    Directory.CreateDirectory(SitecoreDir + @"\" + doc);
                }
                var toDoc = Path.GetFileName(fileName);
                File.Copy(Path.Combine(fileName), Path.Combine(docDir+@"\"+toDoc), true);
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred in AddFile() " + ex.Message);
                Logger.Error(ex.InnerException);
                if (ex.InnerException == null) return;
                Logger.Error("INNER INNER EXCEPTION: " + ex.InnerException.InnerException);

                if (ex.InnerException.InnerException != null)
                    Logger.Error("INNER INNER INNER EXCEPTION: " +
                                ex.InnerException.InnerException.InnerException);
            }
        }

    }
}
