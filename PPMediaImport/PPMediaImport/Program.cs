﻿using NLog;
using PPMediaImport.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace PPMediaImport
{
    class Program
    {
        private static readonly string To = ConfigurationManager.AppSettings["recipient"];
        private static readonly string From = ConfigurationManager.AppSettings["sender"];
        private const string NewLine = "<br/>";
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            try
            {
                var config = new NLog.Config.LoggingConfiguration();
                var logFile = new NLog.Targets.FileTarget("logFile")
                {
                    FileName = $"PPMediaImport-{DateTime.Now:MM-dd-yyyy}.log"
                };
                config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
                LogManager.Configuration = config;

                Console.WriteLine("Import started...");
                Logger.Info("Import started...");

                try
                {
                    Console.WriteLine("Infomatics");
                    Logger.Info("Infomatics");
                    MediaImport.RunInformaticProcesses(); //informatics
                }
                catch (Exception ex)
                {
                    Logger.Info("MediaImport.RunInformaticProcesses()" + ex);
                    var m = new Mail()
                    {
                        From = From,
                        To = To,
                        Body = "PPMediaImport :: Exception occurred while running MediaImport.RunInformaticProcesses()" +
                               NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                               ex.InnerException + NewLine + "Full Exception: " + ex,
                        Subject = "MediaImport.RunInformaticProcesses() Process Failed"
                    };

                    var res = SendMail.SendMailMessage(m);
                    if (res.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Email sent successfully!");
                        Logger.Info("Email sent successfully!");
                    }
                    else
                    {
                        Console.WriteLine("Error sending Email");
                        Logger.Info("Error sending Email");
                    }
                }

                Logger.Info("PPMediaImport :: Completed : " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                Console.WriteLine("Import complete!");
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Logger.Info("Program/Main()" + ex);
                var m = new Mail()
                {
                    From = From,
                    To = To,
                    Body = "PPMediaImport :: Exception occurred while running PP Media Import" +
                           NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                           ex.InnerException + NewLine + "Full Exception: " + ex,
                    Subject = "PP Media Import Process Failed"
                };

                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                Console.WriteLine(ex.Message);
                Console.WriteLine("Program Main Exception: " + ex.ToString());
                Environment.Exit(0);
            }
        }
    }
}
